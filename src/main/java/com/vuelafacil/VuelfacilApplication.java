package com.vuelafacil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VuelfacilApplication {

    public static void main(String[] args) {
        SpringApplication.run(VuelfacilApplication.class, args);
    }

}
