package com.vuelafacil.repository;


import com.vuelafacil.models.Pasajero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasajeroRepository extends JpaRepository<Pasajero,Integer> {

}
