package com.vuelafacil.repository;

import com.vuelafacil.models.EstadoTiquete;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstadoTiqueteRepository extends JpaRepository<EstadoTiquete,Integer> {
}
