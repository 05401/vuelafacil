package com.vuelafacil.repository;

import com.vuelafacil.models.Identificacion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaseTiqueteRepository extends JpaRepository<Identificacion,Integer> {
}
