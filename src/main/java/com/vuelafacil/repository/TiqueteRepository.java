package com.vuelafacil.repository;

import com.vuelafacil.models.Tiquete;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TiqueteRepository extends JpaRepository<Tiquete,Integer> {

}
