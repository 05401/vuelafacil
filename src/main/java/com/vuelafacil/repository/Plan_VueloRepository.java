package com.vuelafacil.repository;

import com.vuelafacil.models.Avion;
import com.vuelafacil.models.Plan_Vuelo;
import com.vuelafacil.models.Tiquete;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Plan_VueloRepository extends JpaRepository<Plan_Vuelo,Integer> {

}
