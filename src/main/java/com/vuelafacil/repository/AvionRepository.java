package com.vuelafacil.repository;

import com.vuelafacil.models.Avion;
import com.vuelafacil.models.Tiquete;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AvionRepository extends JpaRepository<Avion,Integer> {

//    public Avion Guardar(Avion avion)
//    {
//        return repository.save(avion);
//    }

}
