package com.vuelafacil.repository;

import com.vuelafacil.models.Tarifas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TarifasRepository extends JpaRepository<Tarifas,Integer> {
}
