package com.vuelafacil.repository;

import com.vuelafacil.models.ciudades;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ciudadesRepository  extends JpaRepository<ciudades,Integer> {
}