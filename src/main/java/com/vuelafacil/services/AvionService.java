package com.vuelafacil.services;

import com.vuelafacil.models.Avion;
import com.vuelafacil.models.Tiquete;
import com.vuelafacil.repository.AvionRepository;
import com.vuelafacil.repository.TiqueteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AvionService {
    @Autowired
    AvionRepository repository;

    public List<Avion> listar(){
        return repository.findAll();
    }
}
