package com.vuelafacil.services;


import com.vuelafacil.models.Pasajero;
import com.vuelafacil.repository.PasajeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PasajeroService {
    @Autowired
    PasajeroRepository repository;

    public List<Pasajero> listar(){

        return repository.findAll();
    }

    public Pasajero guardar (Pasajero pasajero ){
        return repository.save( pasajero );
    }

    public Pasajero buscarPorId(Integer id){
        return repository.findById(id).get();
    }
    public void Eliminar(Integer id) { repository.deleteById(id);}
}
