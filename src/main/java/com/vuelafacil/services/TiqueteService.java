package com.vuelafacil.services;

import com.vuelafacil.models.Tiquete;
import com.vuelafacil.repository.TiqueteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TiqueteService{
    //CRUD
    @Autowired
    TiqueteRepository repository;

    public List<Tiquete> listar(){
        return repository.findAll();
    }

    //Guardar y Actualizar
    public Tiquete guardar (Tiquete tiquete){
        return repository.save(tiquete);
    }


    //buscar por Id
    public Tiquete buscarPorId(Integer id){
        return repository.findById(id).get();
    }

    //metodo de eliminar
    public void eliminar(Integer id){
        repository.deleteById(id);
    }
}
