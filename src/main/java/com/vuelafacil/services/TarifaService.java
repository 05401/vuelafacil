package com.vuelafacil.services;

import com.vuelafacil.models.Tarifas;
import com.vuelafacil.repository.TarifasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TarifaService {
    @Autowired
    TarifasRepository repository;

    public List<Tarifas> listar(){return repository.findAll();}

    
    //Guardar y Actualizar
    public Tarifas guardar (Tarifas tarifas){
        return repository.save(tarifas);
    }


    //buscar por Id
    public Tarifas buscarPorId(Integer id){
        return repository.findById(id).get();
    }

    //metodo de eliminar
    public void eliminar(Integer id){
        repository.deleteById(id);
    }
}
