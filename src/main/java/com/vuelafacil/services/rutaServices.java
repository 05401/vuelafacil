package com.vuelafacil.services;
import com.vuelafacil.models.ruta;
import com.vuelafacil.repository.rutaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class rutaServices {
    @Autowired
    rutaRepository repository;
    public List<ruta> Listar() {
        return repository.findAll();
    }

    // Guardar y Actualizar
    public ruta guardar(ruta ruta) {
        return repository.save(ruta);
    }

    //buscar por Id
    public ruta buscarPorId(Integer id){
        return repository.findById(id).get();
    }

    //metodo de eliminar
    public void eliminar(Integer id){
        repository.deleteById(id);
    }
}
