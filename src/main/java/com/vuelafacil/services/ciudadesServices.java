package com.vuelafacil.services;
import com.vuelafacil.models.ciudades;
import com.vuelafacil.repository.ciudadesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class ciudadesServices {
    @Autowired
    ciudadesRepository repository;
    public List<ciudades> Listar(){

        return repository.findAll();
    }
    public ciudades guardar (ciudades ciudades){
        return repository.save(ciudades);
    }

    //buscar por Id
    public ciudades buscarPorId(Integer id){
        return repository.findById(id).get();
    }

    //metodo de eliminar
    public void eliminar(Integer id){
        repository.deleteById(id);
    }
}
