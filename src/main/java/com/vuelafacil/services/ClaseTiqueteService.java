package com.vuelafacil.services;

import com.vuelafacil.models.Identificacion;
import com.vuelafacil.repository.ClaseTiqueteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClaseTiqueteService {
    @Autowired
    ClaseTiqueteRepository repository;

    public List<Identificacion> listar() {return repository.findAll();}


}
