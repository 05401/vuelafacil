package com.vuelafacil.services;

import com.vuelafacil.models.EstadoTiquete;
import com.vuelafacil.repository.EstadoTiqueteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstadoTiqueteService {
    @Autowired
    EstadoTiqueteRepository repository;

    public List<EstadoTiquete> listar(){
        return repository.findAll();
    }

    public EstadoTiquete guardar (EstadoTiquete estado){
        return repository.save(estado);
    }


    //buscar por Id
    public EstadoTiquete buscarPorId(Integer id){
        return repository.findById(id).get();
    }

    //metodo de eliminar
    public void eliminar(Integer id){
        repository.deleteById(id);
    }
}
