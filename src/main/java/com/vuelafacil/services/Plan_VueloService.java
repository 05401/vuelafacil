package com.vuelafacil.services;

import com.vuelafacil.models.Plan_Vuelo;
import com.vuelafacil.models.Tiquete;
import com.vuelafacil.repository.Plan_VueloRepository;
import com.vuelafacil.repository.TiqueteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Plan_VueloService {
    @Autowired
    Plan_VueloRepository repository;

    public List<Plan_Vuelo> listar(){
        return repository.findAll();
    }

    //Guardar y Actualizar
    public Plan_Vuelo guardar (Plan_Vuelo plan_vuelo){
        return repository.save(plan_vuelo);
    }


    //buscar por Id
    public Plan_Vuelo buscarPorId(Integer id){
        return repository.findById(id).get();
    }

    //metodo de eliminar
    public void eliminar(Integer id){
        repository.deleteById(id);
    }
}
