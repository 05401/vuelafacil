package com.vuelafacil.controllers;


import com.vuelafacil.models.EstadoTiquete;
import com.vuelafacil.services.EstadoTiqueteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EstadoTiqueteController {
    @Autowired
    EstadoTiqueteService service;
    @GetMapping({"/estado-tiquete"})
    public String Vuelos(Model model){
        model.addAttribute("estadoDelTiquete",service.listar());
        return "/Tiquete/estadoTiquete";
    }
    @GetMapping({"/estado-tiquete/crear"})
    public String estadoTiqueteCrear( Model model){
        EstadoTiquete estados=new EstadoTiquete();
        model.addAttribute("estadoDelTiquete" ,estados);
        return "/Tiquete/crearEstadoTiquete";
    }
    @PostMapping({"/estado-tiquete/guardar"})
    public String estadoTiqueteGuardar(@ModelAttribute("estadoDelTiquete") EstadoTiquete estados){
        service.guardar(estados);
        return "redirect:/estado-tiquete";
    }

    //editar ;
    @GetMapping({"/estado-tiquete/editar/{id}"})
    public String estadoTiqueteEditar(@PathVariable int id, Model model){
        EstadoTiquete estados=service.buscarPorId(id);
        model.addAttribute("estadoDelTiquete",estados);
        return "/Tiquete/crearEstadoTiquete";
    }

    @GetMapping({"/estado-tiquete/eliminar/{id}"})
    public String estadoTiqueteEliminar(@PathVariable int id){
        service.eliminar(id);
        return "redirect:/estado-tiquete";
    }
}
