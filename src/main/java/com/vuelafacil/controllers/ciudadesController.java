package com.vuelafacil.controllers;
import com.vuelafacil.models.ciudades;
import com.vuelafacil.services.ciudadesServices;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ciudadesController {
    @Autowired
    ciudadesServices service;
    @GetMapping({"/ciudades"})
    public String ciudades(Model model){
        model.addAttribute("ciudades",service.Listar());
        return "/controladorDeRutas/ciudades";
    }
    @GetMapping({"/ciudades/crear"})
    public String ciudadesCrear( Model model){
        ciudades Ciudades=new ciudades();
        model.addAttribute("ciudades" ,Ciudades);
        return "/ciudades/create";
    }
    @PostMapping({"/ciudades/guardar"})
    public String ciudadesGuardar(@ModelAttribute("ciudades") ciudades Ciudades){
        service.guardar(Ciudades);
        return "redirect:/ciudades";
    }
    //editar ;
    @GetMapping({"/ciudades/editar/{id}"})
    public String ciudadesEditar(@PathVariable int id, Model model) {
         ciudades Ciudades = service.buscarPorId(id);
        model.addAttribute("ciudades", Ciudades);
        return "/ciudades/create";
    }
        //eliminar
        @GetMapping({"/ciudades/eliminar/{id}"})
        public String ciudadesEliminar( @PathVariable int id){
            service.eliminar(id);
            return "redirect:/ciudades";
        }
}