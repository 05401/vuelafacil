package com.vuelafacil.controllers;

import com.vuelafacil.models.Pasajero;
import com.vuelafacil.services.PasajeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PasajeroController {
    @Autowired
    PasajeroService service;
    @GetMapping({"/pasajero"})
    public  String pasajero(Model model){
        model.addAttribute("pasajero",service.listar());
        return "/usuario/indexPasajero";
    }
    @PostMapping({"/pasajero/guardar"})
    public String PasajeroGuardar(@ModelAttribute("pasajero") Pasajero pasajero) {
        service.guardar(pasajero);
        return "redirect:/pasajero";
    }
    @GetMapping({"/pasajero/crear"})
    public String PasajeroCrear( Model model){
        Pasajero pasajero=new Pasajero();
        model.addAttribute("pasajero" ,pasajero);
        return "/usuario/datosUsuario";
    }
    @GetMapping({"/pasajero/edit/{id}"})
    public String Editar(@PathVariable int id, Model model){
        Pasajero pasajero= service.buscarPorId(id);
        model.addAttribute("pasajero",pasajero);
        return "/usuario/datosUsuario";
    }
    @GetMapping({"/pasajero/delete/{id}"})
    public String Eliminar(@PathVariable int id) {
        service.Eliminar(id);
        return "redirec:/pasajero";
    }
}
