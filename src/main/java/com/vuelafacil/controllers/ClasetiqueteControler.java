package com.vuelafacil.controllers;

import com.vuelafacil.models.Tiquete;
import com.vuelafacil.services.ClaseTiqueteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ClasetiqueteControler {
    @Autowired
    ClaseTiqueteService service;
    @GetMapping({"/clase-tiquete"})
    public  String clase_tiquete (Model model){
        model.addAttribute("clasetiquete",service.listar());
        return "/Tiquete/ClaseTiquete";
    }


}
