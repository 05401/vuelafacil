package com.vuelafacil.controllers;

import com.vuelafacil.models.Avion;
import com.vuelafacil.services.AvionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AvionController {
    //Mapear Ruta para mostrar el listado de aviones
    @Autowired
    AvionService service;
    @GetMapping({"/aviones"})
    public String Aviones(Model model){
        model.addAttribute("aviones",service.listar());
        return "/Tiquete/Aviones";
    }

    //Ruta para formulario Nuevo
    @GetMapping({"/avion/create"})
    public String Nuevo(Model model){
        Avion avion = new Avion();
        model.addAttribute("avion",avion);
        return "/Avion/create";
    }

    //Ruta para guardar
//    @PostMapping({"/avion/guardar"})
//    public String Guardar(@ModelAttribute("avion") Avion avion){
//        service.Guardar(avion);
//        return "redirect:/avion/list";
//    }

}
