package com.vuelafacil.controllers;
import com.vuelafacil.models.ruta;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import com.vuelafacil.services.rutaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class rutaController {

    @Autowired
    rutaServices service;
    @GetMapping({"/ruta"})
    public String ruta(Model model){
        model.addAttribute("ruta",service.Listar());
        return "/ControladordeRutas/ruta";
    }
    @GetMapping({"/ruta/crear"})
    public String RutaCrear( Model model){
        ruta  Ruta=new ruta();
        model.addAttribute("ruta",Ruta);
        return "/ControladordeRutas/crearRuta";
    }
    @PostMapping({"/ruta/guardar"})
    public String rutaGuardar(@ModelAttribute("ruta") ruta Ruta){
        service.guardar(Ruta);
        return "redirect:/ruta";
    }
    //editar ;
    @GetMapping({"/ruta/editar/{id}"})
    public String RutaEditar(@PathVariable int id, Model model) {
        ruta Ruta = service.buscarPorId(id);
        model.addAttribute("ruta", Ruta);
        return "/ControladordeRutas/crearRuta";
    }
    //eliminar
    @GetMapping({"/ruta/eliminar/{id}"})
    public String rutaEliminar( @PathVariable int id){
        service.eliminar(id);
        return "redirect:/ruta";
    }
}