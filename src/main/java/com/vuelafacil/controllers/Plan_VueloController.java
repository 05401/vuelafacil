package com.vuelafacil.controllers;

import com.vuelafacil.models.Plan_Vuelo;
import com.vuelafacil.models.Tiquete;
import com.vuelafacil.services.Plan_VueloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Plan_VueloController {
    //Mapear Ruta para mostrar el listado de Plan de Vuelo
    @Autowired
    Plan_VueloService service;
    @GetMapping({"/Plan_Vuelo"})
    public String Plan_Vuelo(Model model){
        model.addAttribute("plan_vuelo",service.listar());
        return "/controladorDeRutas/indexControlador";
    }

    @GetMapping({"/Plan_Vuelo/crear"})
    public String Plan_VueloCrear( Model model){
        Plan_Vuelo plan_vuelo=new Plan_Vuelo();
        model.addAttribute("plan_vuelo" ,plan_vuelo);
        return "/controladorDeRutas/createPlan";
    }
    @PostMapping({"/Plan_Vuelo/guardar"})
    public String Plan_VueloGuardar(@ModelAttribute("plan_vuelo") Plan_Vuelo plan_vuelo){
        service.guardar(plan_vuelo);
        return "redirect:/Plan_Vuelo";
    }

    //editar ;
    @GetMapping({"/Plan_Vuelo/editar/{id}"})
    public String Plan_VueloEditar(@PathVariable int id, Model model){
        Plan_Vuelo plan_vuelo=service.buscarPorId(id);
        model.addAttribute("plan_vuelo",plan_vuelo);
        return "/controladorDeRutas/createPlan";
    }

    //eliminar
    @GetMapping({"/Plan_Vuelo/eliminar/{id}"})
    public String Plan_VueloEliminar(@PathVariable int id){
        service.eliminar(id);
        return "redirect:/Plan_Vuelo";
    }
}
