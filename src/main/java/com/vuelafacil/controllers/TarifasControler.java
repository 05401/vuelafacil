package com.vuelafacil.controllers;

import com.vuelafacil.models.Tarifas;
import com.vuelafacil.services.TarifaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TarifasControler {
    @Autowired
    TarifaService service;
    @GetMapping({"/tarifa"})
    public  String tarifas(Model model){
        model.addAttribute("tarifas",service.listar());
        return "/Tiquete/Tarifa";
    }
    @GetMapping({"/tarifa/crear"})
    public String tarifasCrear( Model model){
        Tarifas tarifas=new Tarifas();
        model.addAttribute("tarifas" ,tarifas);
        return "/Tiquete/crearTarifa";
    }
    @PostMapping({"/tarifa/guardar"})
    public String tarifasGuardar(@ModelAttribute("tarifas") Tarifas tarifass){
        service.guardar(tarifass);
        return "redirect:/tarifa";
    }

    //editar ;
    @GetMapping({"/tarifa/editar/{id}"})
    public String tarifasEditar(@PathVariable int id, Model model){
        Tarifas tarifas=service.buscarPorId(id);
        model.addAttribute("tarifas",tarifas);
        return "/Tiquete/crearTarifa";
    }

    //eliminar
    @GetMapping({"/tarifa/eliminar/{id}"})
    public String tarifasEliminar(@PathVariable int id){
        service.eliminar(id);
        return "redirect:/tarifa";
    }
}
