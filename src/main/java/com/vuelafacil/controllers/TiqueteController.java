package com.vuelafacil.controllers;

import com.vuelafacil.models.Tiquete;
import com.vuelafacil.services.TiqueteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TiqueteController {
    //Mapear Ruta para mostrar el listado de vuelos
    @Autowired
    TiqueteService service;
    @GetMapping({"/tiquete"})
    public String Vuelos(Model model){
        model.addAttribute("vuelos",service.listar());
        return "/Tiquete/tiquete";
    }
    @GetMapping({"/tiquete/crear"})
    public String VuelosCrear( Model model){
        Tiquete tiquete=new Tiquete();
        model.addAttribute("vuelos" ,tiquete);
        return "/Tiquete/create";
    }
    @PostMapping({"/tiquete/guardar"})
    public String VuelosGuardar(@ModelAttribute("vuelos") Tiquete tiquete){
        service.guardar(tiquete);
        return "redirect:/tiquete";
    }

    //editar ;
    @GetMapping({"/tiquete/editar/{id}"})
    public String VuelosEditar(@PathVariable int id, Model model){
        Tiquete tiquete=service.buscarPorId(id);
        model.addAttribute("vuelos",tiquete);
        return "/Tiquete/create";
    }

    //eliminar
    @GetMapping({"/tiquete/eliminar/{id}"})
    public String VuelosEliminar(@PathVariable int id){
        service.eliminar(id);
        return "redirect:/tiquete";
    }

}
