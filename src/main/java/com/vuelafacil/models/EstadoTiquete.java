package com.vuelafacil.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="estado_tiquete")
public class EstadoTiquete {
    @Id
    private int cod_etiq;
    private String estado;
    private String descrip;
}
