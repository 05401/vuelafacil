package com.vuelafacil.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@Entity
@Table(name="tipo_identificacion")
public class Identificacion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_iden;
    private String tip_ident;
}
