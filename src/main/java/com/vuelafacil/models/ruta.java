package com.vuelafacil.models;


import lombok.Setter;
import lombok.Getter;
import javax.persistence.*;



@Getter
@Setter
@Entity
@Table(name="ruta")
public class ruta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_ruta;
    private int cod_corig;
    private int cod_cdes;
    private int dura;
}