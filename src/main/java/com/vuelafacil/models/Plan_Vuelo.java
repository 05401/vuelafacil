package com.vuelafacil.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Getter
@Setter
@Entity
@Table(name="plan_vuelo")
public class Plan_Vuelo {
    @Id
    private int id_pvue;
    private int id_ruta;
    private int cod_avion;
    private Date fec_viaje;
    private String hor_sali;
    private String hor_llega;
}
