package com.vuelafacil.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="avion")
public class Avion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_avion;
    private int ubic;
    private int modelo;
    private int capa_plus;
    private int cap_ligmed;
}
