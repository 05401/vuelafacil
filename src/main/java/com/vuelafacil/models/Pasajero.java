package com.vuelafacil.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="pasajero")
public class Pasajero {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_pasa;
    private int cod_iden;
    private String nom_comp;
    private String corr;
    private String tele;
}
