package com.vuelafacil.models;

import lombok.Setter;
import lombok.Getter;
import javax.persistence.*;
@Getter
@Setter
@Entity
@Table(name="ciudades")
public class ciudades {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int cod_ciud;
    private String aerop;
    public String nombre;


}
