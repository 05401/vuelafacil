package com.vuelafacil.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="clase_tiquete")
public class ClaseTiquete {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_ctiq;
    private String clase;
    private int num_male;
    private String desc;
}
