package com.vuelafacil.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="tiquete")
public class Tiquete {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_tiqu;
    private int id_pasa;
    private int id_pvue;
    private int cod_ctiq;
    private int cod_etiq;
    private int cod_tari;
    private int pue_asig;
}
