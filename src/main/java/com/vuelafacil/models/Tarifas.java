package com.vuelafacil.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="tarifas")
public class Tarifas {
    @Id
    private int cod_tarifa;
    private int id_ruta;
    private int cod_ctiq;
    private double costo;

}
