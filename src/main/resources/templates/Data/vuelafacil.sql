-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2022 a las 04:13:49
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vuelafacil`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `avion`
--

CREATE TABLE `avion` (
  `cod_avion` int(10) NOT NULL,
  `ubic` int(10) NOT NULL,
  `modelo` varchar(10) NOT NULL,
  `capa_plus` int(5) NOT NULL,
  `cap_ligmed` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `cod_ciud` int(10) NOT NULL,
  `aerop` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase_tiquete`
--

CREATE TABLE `clase_tiquete` (
  `cod_ctiq` int(10) NOT NULL,
  `clase` varchar(30) NOT NULL,
  `num_male` int(10) NOT NULL,
  `desc` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_tiquete`
--

CREATE TABLE `estado_tiquete` (
  `cod_etiq` int(10) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `descrip` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasajero`
--

CREATE TABLE `pasajero` (
  `id_pasa` int(10) NOT NULL,
  `cod_iden` int(10) NOT NULL,
  `nom_comp` varchar(170) NOT NULL,
  `corr` varchar(100) DEFAULT NULL,
  `tele` varchar(15) NOT NULL,
  `dire` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_vuelo`
--

CREATE TABLE `plan_vuelo` (
  `id_pvue` int(10) NOT NULL,
  `id_ruta` int(10) NOT NULL,
  `cod_avion` int(10) NOT NULL,
  `fec_viaje` date NOT NULL,
  `hor_sali` time(6) NOT NULL,
  `hor_llega` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta`
--

CREATE TABLE `ruta` (
  `id_ruta` int(10) NOT NULL,
  `cod_corig` int(10) NOT NULL,
  `cod_cdes` int(10) NOT NULL,
  `dura` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifas`
--

CREATE TABLE `tarifas` (
  `cod_tarifa` int(10) NOT NULL,
  `id_ruta` int(10) NOT NULL,
  `cod_ctiq` int(10) NOT NULL,
  `costo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_identificacion`
--

CREATE TABLE `tipo_identificacion` (
  `cod_iden` int(10) NOT NULL,
  `tip_ident` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiquete`
--

CREATE TABLE `tiquete` (
  `id_tiqu` int(10) NOT NULL,
  `id_pasa` int(10) NOT NULL,
  `id_pvue` int(10) NOT NULL,
  `cod_ctiq` int(10) NOT NULL,
  `cod_etiq` int(10) NOT NULL,
  `cod_tari` int(10) NOT NULL COMMENT 'tarifa',
  `pue_asig` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `avion`
--
ALTER TABLE `avion`
  ADD PRIMARY KEY (`cod_avion`),
  ADD UNIQUE KEY `cod_cplus` (`capa_plus`),
  ADD UNIQUE KEY `ubic` (`ubic`);

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`cod_ciud`);

--
-- Indices de la tabla `clase_tiquete`
--
ALTER TABLE `clase_tiquete`
  ADD PRIMARY KEY (`cod_ctiq`),
  ADD KEY `clase` (`clase`);

--
-- Indices de la tabla `estado_tiquete`
--
ALTER TABLE `estado_tiquete`
  ADD PRIMARY KEY (`cod_etiq`);

--
-- Indices de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD PRIMARY KEY (`id_pasa`),
  ADD KEY `cod_iden` (`cod_iden`);

--
-- Indices de la tabla `plan_vuelo`
--
ALTER TABLE `plan_vuelo`
  ADD PRIMARY KEY (`id_pvue`),
  ADD KEY `id_ruta` (`id_ruta`),
  ADD KEY `cod_avion` (`cod_avion`);

--
-- Indices de la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD PRIMARY KEY (`id_ruta`),
  ADD KEY `cod_corig` (`cod_corig`),
  ADD KEY `cod_cdes` (`cod_cdes`);

--
-- Indices de la tabla `tarifas`
--
ALTER TABLE `tarifas`
  ADD PRIMARY KEY (`cod_tarifa`),
  ADD KEY `id_ruta` (`id_ruta`),
  ADD KEY `cod_ctiq` (`cod_ctiq`);

--
-- Indices de la tabla `tipo_identificacion`
--
ALTER TABLE `tipo_identificacion`
  ADD PRIMARY KEY (`cod_iden`);

--
-- Indices de la tabla `tiquete`
--
ALTER TABLE `tiquete`
  ADD PRIMARY KEY (`id_tiqu`),
  ADD UNIQUE KEY `prec` (`cod_tari`),
  ADD KEY `id_pasa` (`id_pasa`),
  ADD KEY `id_pvue` (`id_pvue`),
  ADD KEY `cod_ctiq` (`cod_ctiq`),
  ADD KEY `cod_etiq` (`cod_etiq`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clase_tiquete`
--
ALTER TABLE `clase_tiquete`
  MODIFY `cod_ctiq` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  MODIFY `id_pasa` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ruta`
--
ALTER TABLE `ruta`
  MODIFY `id_ruta` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tiquete`
--
ALTER TABLE `tiquete`
  MODIFY `id_tiqu` int(10) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `avion`
--
ALTER TABLE `avion`
  ADD CONSTRAINT `avion_ibfk_1` FOREIGN KEY (`ubic`) REFERENCES `ciudades` (`cod_ciud`);

--
-- Filtros para la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD CONSTRAINT `pasajero_ibfk_1` FOREIGN KEY (`cod_iden`) REFERENCES `tipo_identificacion` (`cod_iden`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `plan_vuelo`
--
ALTER TABLE `plan_vuelo`
  ADD CONSTRAINT `plan_vuelo_ibfk_1` FOREIGN KEY (`id_ruta`) REFERENCES `ruta` (`id_ruta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `plan_vuelo_ibfk_2` FOREIGN KEY (`cod_avion`) REFERENCES `avion` (`cod_avion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD CONSTRAINT `ruta_ibfk_1` FOREIGN KEY (`cod_corig`) REFERENCES `ciudades` (`cod_ciud`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ruta_ibfk_2` FOREIGN KEY (`cod_cdes`) REFERENCES `ciudades` (`cod_ciud`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tarifas`
--
ALTER TABLE `tarifas`
  ADD CONSTRAINT `tarifas_ibfk_1` FOREIGN KEY (`id_ruta`) REFERENCES `ruta` (`id_ruta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tarifas_ibfk_2` FOREIGN KEY (`cod_ctiq`) REFERENCES `clase_tiquete` (`cod_ctiq`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tiquete`
--
ALTER TABLE `tiquete`
  ADD CONSTRAINT `tiquete_ibfk_1` FOREIGN KEY (`id_pasa`) REFERENCES `pasajero` (`id_pasa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tiquete_ibfk_2` FOREIGN KEY (`cod_ctiq`) REFERENCES `clase_tiquete` (`cod_ctiq`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tiquete_ibfk_3` FOREIGN KEY (`id_pvue`) REFERENCES `plan_vuelo` (`id_pvue`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tiquete_ibfk_4` FOREIGN KEY (`cod_etiq`) REFERENCES `estado_tiquete` (`cod_etiq`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tiquete_ibfk_5` FOREIGN KEY (`cod_tari`) REFERENCES `tarifas` (`cod_tarifa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
